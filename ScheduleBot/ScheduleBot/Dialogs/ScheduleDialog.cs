﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using System.Linq;

namespace ScheduleBot.Dialogs
{
	[Serializable]
	public class ShowScheduleDialog : IDialog<object>
	{
		public async Task StartAsync(IDialogContext context)
		{
			context.Wait(MessageReceivedAsync);
		}

		private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
		{
			var activity = await result;
			var message = (activity.Text ?? string.Empty).ToLower();
			Attachment schedule = null;

			if (message.Contains("today"))
			{
				schedule = new ThumbnailCard
				{
					Title = "Schedule",
					Subtitle = "This is your first schedule for michael",
					Text = "This is your schedule for michael starts at 0800 ends at 0830",
					Images = new List<CardImage> { new CardImage("https://sec.ch9.ms/ch9/7ff5/e07cfef0-aa3b-40bb-9baa-7c9ef8ff7ff5/buildreactionbotframework_960.jpg") },
					Buttons = new List<CardAction> { new CardAction(ActionTypes.OpenUrl, "Get Started", value: "https://docs.microsoft.com/bot-framework") }
				}.ToAttachment();
				await context.PostAsync($"Here is your schedule for today.");
			}
			else if (message.Contains("tomorrow"))
			{
			    schedule = new ThumbnailCard
			    {
			        Title = "Schedule",
			        Subtitle = "This is your second schedule for michael",
			        Text = "This is your schedule for michael starts at 0800 ends at 0830",
			        Images = new List<CardImage> { new CardImage("https://sec.ch9.ms/ch9/7ff5/e07cfef0-aa3b-40bb-9baa-7c9ef8ff7ff5/buildreactionbotframework_960.jpg") },
			        Buttons = new List<CardAction> { new CardAction(ActionTypes.OpenUrl, "Get Started", value: "https://docs.microsoft.com/bot-framework") }
			    }.ToAttachment();
			    await context.PostAsync($"Here is your schedule for tomorrow.");
			}
			else
			{
			    await context.PostAsync($"Sorry, what schedule did you want?");
			}
			
			context.Done(schedule);
		}
	}
}