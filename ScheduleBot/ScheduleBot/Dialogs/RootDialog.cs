﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;

namespace ScheduleBot.Dialogs
{
	[Serializable]
	public class RootDialog : IDialog<object>
	{

		public async Task StartAsync(IDialogContext context)
		{
			await context.PostAsync($"Welcome to EziScheduleBot");
			
			context.Wait(MessageReceivedAsync);
		}
		
		private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
		{
			var activity = await result;
			var message = (activity.Text ?? string.Empty).ToLower();

			if (message.Contains("show") && message.Contains("schedule"))
			{
				await context.Forward(new ShowScheduleDialog(), this.ResumeAfterScheduleDialog, activity, CancellationToken.None);
			}
		}

		private async Task ResumeAfterScheduleDialog(IDialogContext context, IAwaitable<object> result)
		{
			var scheduleCard = await result as Attachment;
			var message = context.MakeMessage();

            if (scheduleCard != null)
		    {
		        message.Attachments.Add(scheduleCard);
		        await context.PostAsync(message);
		    }

		    context.Wait(this.MessageReceivedAsync);
		}
	}
}