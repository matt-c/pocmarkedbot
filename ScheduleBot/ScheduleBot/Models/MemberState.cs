﻿using System;
using Microsoft.Bot.Connector;

namespace ScheduleBot.Models
{
	public class MemberState
	{
		public ChannelAccount Member { get; set; }
		public bool Connected { get; private set; }
		
		public MemberState(ChannelAccount member)
		{
			Member = member ?? throw new ArgumentNullException(nameof(member));
			Connected = true;
		}

		public string GetId()
		{
			return Member.Id;
		}

		public void Disconnect()
		{
			Connected = false;
		}
	}
}