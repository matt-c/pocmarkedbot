﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using ScheduleBot.Models;
using ScheduleBot.Dialogs;

namespace ScheduleBot
{
	[BotAuthentication]
	public class MessagesController : ApiController
	{
		private IList<MemberState> _memberState = new List<MemberState>();

		/// <summary>
		/// POST: api/Messages
		/// Receive a message from a user and reply to it
		/// </summary>
		public async Task<HttpResponseMessage> Post([FromBody]Activity activity)
		{
			if (activity.Type == ActivityTypes.Message)
			{
				await Conversation.SendAsync(activity, () => new RootDialog());
			}
			else
			{
				HandleSystemMessage(activity);
				
				await SendWelcomeMessages(activity);
			}
			var response = Request.CreateResponse(HttpStatusCode.OK);
			return response;
		}
		
		private async Task SendWelcomeMessages(Activity activity)
		{
			if (activity.Type == ActivityTypes.ConversationUpdate)
			{
				foreach (var member in _memberState.Where(m => activity.MembersAdded.Any(x => x.Id == m.GetId())))
				{
					await Conversation.SendAsync(activity, () => new RootDialog());
				}
			}
		}

		private Activity HandleSystemMessage(Activity message)
		{
			if (message.Type == ActivityTypes.DeleteUserData)
			{
				// Implement user deletion here
				// If we handle user deletion, return a real message
			}
			else if (message.Type == ActivityTypes.ConversationUpdate)
			{
				foreach (var member in message.MembersAdded.Where(m => m.Id != message.Recipient.Id))
				{
					var state = new MemberState(member);
					_memberState.Add(state);
				}
				
				foreach (var member in _memberState.Where(m => message.MembersRemoved.Any(x => x.Id == m.GetId())))
				{
					member.Disconnect();
				}
			}
			else if (message.Type == ActivityTypes.ContactRelationUpdate)
			{
				// Handle add/remove from contact lists
				// Activity.From + Activity.Action represent what happened
			}
			else if (message.Type == ActivityTypes.Typing)
			{
				// Handle knowing tha the user is typing
			}
			else if (message.Type == ActivityTypes.Ping)
			{
			}

			return null;
		}
	}
}